import { Component, OnInit } from '@angular/core';
import { CalculatorService } from '../calculator.service';
import { Observable } from 'rxjs';
import { Instalment } from './instalment';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-loan',
  templateUrl: './loan.component.html',
  styleUrls: ['./loan.component.css']
})
export class LoanComponent implements OnInit {

  instalments$: Observable<Instalment[]>
  loanCalculationForm: FormGroup;
  formValidatedAmount: boolean;
  formValidatedPeriod: boolean;
  formValid: boolean;
  constructor(private calculatorService: CalculatorService) { 
    this.formValid = true;
    this.formValidatedAmount = true;
    this.formValidatedPeriod = true;
    this.loanCalculationForm = new FormGroup({
      amount: new FormControl('100000',{
        updateOn: 'change',
        validators: [Validators.min(1),Validators.pattern('^[0-9]+$'),Validators.required]
      }),
      paybackInYears: new FormControl('10',{
        updateOn: 'change',
        validators: [Validators.min(1),Validators.pattern('^[0-9]+$'),Validators.required]
      })
    });
  }

  ngOnInit() {
    this.loanCalculationForm.controls['amount'].statusChanges.subscribe(
      (status) =>
      {       
        if(status === 'INVALID')
        {
          this.formValidatedAmount = false;
        }
        else
        {
          this.formValidatedAmount = true;
        }
        this.formValid = this.formValidatedAmount && this.formValidatedPeriod;
      });

      this.loanCalculationForm.controls['paybackInYears'].statusChanges.subscribe(
        (status) =>
        {
          if(status === 'INVALID')
          {
            this.formValidatedPeriod = false;
          }
          else
          {
            this.formValidatedPeriod = true;
          }
          this.formValid = this.formValidatedAmount && this.formValidatedPeriod;
        });
  }


  handleReactiveSubmit(form: FormGroup) {
    this.instalments$ = this.calculatorService.calculate(form.value.amount, form.value.paybackInYears, '3.5');
  }

}
