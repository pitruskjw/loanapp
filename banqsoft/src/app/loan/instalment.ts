export interface Instalment {
    instalmentNumber: number;
    interest: number;
    corpus: number;
  }
  