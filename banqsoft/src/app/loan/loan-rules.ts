export interface LoanRules
{
    amount: Number;
    paybackTimeInYears: Number;
}