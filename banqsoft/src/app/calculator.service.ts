import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Instalment } from './loan/instalment';

@Injectable({
  providedIn: 'root'
})
export class CalculatorService {

  constructor(private http: HttpClient) { }

  calculate(amunt: string, paybackInYears: string, rateOfInterest: string) {

    let param = new HttpParams().set('amount', amunt).set('paybackInYears', paybackInYears).set('rateOfInterest',rateOfInterest);

    return this.http.get<Instalment[]>('http://localhost:63686/api/LoanCalculator', {params: param});
  }
}
