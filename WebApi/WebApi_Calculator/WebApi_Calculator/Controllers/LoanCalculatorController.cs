﻿namespace WebApi_Calculator.Controllers
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Net;
  using System.Net.Http;
  using System.Web.Http;
  using System.Web.Http.Cors;
  using WebApi_Calculator.Models;

  [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
  public class LoanCalculatorController : ApiController
  {
    public IEnumerable<Instalment> Get(int amount, int paybackInYears, string rateOfInterest)
    {
      IMortageLoanCalculator c = new DecreasingInstallmentsLoanCalculator();
      return c.Calculate(amount, paybackInYears, rateOfInterest);
    }
  }
}
