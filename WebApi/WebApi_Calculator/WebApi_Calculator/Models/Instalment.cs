﻿namespace WebApi_Calculator.Models
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Web;
  public class Instalment
  {
    public string instalmentNumber { get; set; }
    public decimal interest { get; set; }
    public decimal corpus { get; set; }
  }
}