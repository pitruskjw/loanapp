﻿namespace WebApi_Calculator.Models
{
  using System;
  using System.Collections.Generic;
  using System.Globalization;
  using System.Linq;
  using System.Web;

  public class DecreasingInstallmentsLoanCalculator : IMortageLoanCalculator
  {
    public IEnumerable<Instalment> Calculate(int amount, int paybackInYears, string rateOfInterest)
    {
      decimal rateOfInterestDecimal = Decimal.Parse(rateOfInterest, CultureInfo.InvariantCulture);
      decimal corpus = (amount / (decimal)(paybackInYears * 12));
      List<Instalment> instalments = new List<Instalment>();

      for (int i = 0; i < paybackInYears * 12; i++)
      {
        decimal interest = (amount - (i * corpus)) * (rateOfInterestDecimal / 100M) / 12M;
        instalments.Add(new Instalment() { instalmentNumber = (i + 1).ToString(), interest = interest, corpus = corpus });
      }

      return instalments;
    }
  }
}